﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject bull;
    [SerializeField]
    private Transform posInstan;
    [SerializeField]
    private GameObject GOBullets;


    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.1f);
            transform.LookAt(transform.position + Vector3.forward);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.1f);
            transform.LookAt(transform.position + Vector3.back);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = new Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            transform.LookAt(transform.position + Vector3.right);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = new Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z);
            transform.LookAt(transform.position + Vector3.left);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            GameObject bullShot = Instantiate(bull,posInstan.position,posInstan.rotation,GOBullets.transform);
            bullShot.GetComponent<Rigidbody>().velocity = transform.forward*5;
            
            
        }
    }
}
